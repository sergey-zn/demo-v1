# ZeroNorth Microservice

This is an API Service that . 

## Project dependencies outline

* Node.js 14.9.0
* Typescript 3.x
* Express 4.x

## Installation

1. Install nvm

   * Bash -- <https://github.com/nvm-sh/nvm>
   * Zsh -- <https://github.com/lukechilds/zsh-nvm> (recommended)

2. `nvm use` or `nvm install`

    * This will get the correct version of node running in your current terminal instance

3. This project includes submodule that holds shared [configuration](https://gitlab.com/zeronorth/engineering/library/typescript-service-config). AFTER you created the project using cookiecutter run:
    * Initialize submodule with `git submodule add git@gitlab.com:zeronorth/engineering/library/typescript-service-config.git config`
    * Change the path in the `.gitmodules` file to be relative, smth like `url = ../../library/typescript-service-config.git`, otherwise gitlab pipeline will fail.
    * Later to update submodules you can run `git submodule update`

4. In your project root, run `npm i` to install local dependencies for typescript

5. Specify environmental variables in `.env` file at the root directory (Rename `.env.tmpl` and fill it out with what you need).


## Important Commands

* `make test` or `npm t` Runs all the tests, code coverage and linting.  If it fails here, it will fail in deployment
* Everytime you'd like to update shared configuration run `git submodule update`


## Directory Structure

```none
<root>
  + /config                     # Submodule with shared configuration
  + /src                        # Root for all .ts source files to be transpiled
  +     /controllers            # Definitions for controllers
  +     /models                 # Definitions for models (they drive models defined in OpenAPI specification) 
  + /test                       # Root for all .ts test files
  .gitignore                    # List of files/paths for git to ignore
  .nvmrc                        # Instructs NVM which version of node to use
  .eslintrc.json                # Main ESLint configuration
  jest.config.js                # Configuration for jest (testing framework)
  Makefile                      # Build Targets
  package.json                  # Project Metadata
  tsconfig.json                 # Configuration for TypeScript compiler (based on shared configuration)
  tsconfig.lint.json            # Typescript compiler options for ESLint
```

## How to add new end-point

1. Add new controller into `src/controllers/v1` folder
    
    * Add [typescript-rest](https://github.com/thiagobustamante/typescript-rest/wiki/Overview#basic-usage) annotations to the controller and to the controller methods
    * Define corresponding domain shape as an interface within `models/` folder. In general, it will follow the shape of the data coming from the database
    * Add [typescript-rest-swagger]() annotations to enhance swagger documentation. The one you'd need for sure is `@Tags(<your tag>)`
2. Expose your controller within `src/controllers/v1/index.ts` file 
3. Use VSCode launch config to start a server. That will run build step which includes Typescript compilation and swagger documentation generation
4. Add tests. Follow the patterns in [issues-api](https://gitlab.com/zeronorth/engineering/platform/issues-api) and [lumbergh](https://gitlab.com/zeronorth/engineering/platform/lumbergh) repositories.
 
## If you need to work with local zn-repository (brace, brace)

1. Install `tsyringe` globally. `npm i tsyringe -g`
2. Link `tsyringe` inside `zn-repository` `npm link tsyringe`
3. Expose `zn-repository` for linking by running `npm link` from there
4. Use `make watch` inside zn-repository, to re-compile `dist` folder based on the changes you are making
5. Link both `tsyringe` and `zn-repository` in this project `npm link @zeronorth/zn-repository tsyringe` 
6. _PAIN:_ Everytime you do `npm istall` for package or project it removes local links (Expected behaviour since npm v5). So..., you need to re-link (by running #4) everytime after that.

## How everything works together
* When you start a server it builds express routes based on the list of controllers exposed in `src/controllers/v1/index.ts` folder
* `typescript-rest-swagger` takes care of creating OpenAPI specification based on controller annotations during compilation via `npm run build`. The spec is located at `dist/swagger.json` file
* `typescript-openapi-validator` makes sure that requests and(!) responses adhere to the generated OpenAPI spec file. If response from the controller method doesn't match you'd get 500 error.

* `typescript-rest-swagger` determines data shapes based on the types used an input and return values for controller functions. It cannot (for good reasons) pickup shapes from installed packages, such as zn-repository. Which means you need to have your domain model declarations and possibly transformations of zn-repository output to match your domain models.


## TODO

* [ ] enable watch mode (could be done for TS, but we also want swagger docs update)
* [ ] custome Jest matcher for supertest responses

