/*!
 * Copyright © 2020 ZeroNorth, Inc. All Rights Reserved.
 *
 * All information, in plain text or obfuscated form, contained herein
 * is, and remains the property of ZeroNorth, Inc. and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to ZeroNorth, Inc. and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ZeroNorth, Inc. (support@zeronorth.io)
 *
 */
import request from 'supertest';
import { initializeApp, Application } from '@zeronorth/api-service';
import apiSpec from '../src/apiSpec';

describe('Documentation', () => {
  let app: Application;

  beforeAll(async () => {
    app = await initializeApp(apiSpec);
  });

  it('Should process redirect from documentation page', async () => {
    const res = await request(app).get('/api/v1/documentation');
    expect(res.status).toEqual(301);
  });
});
