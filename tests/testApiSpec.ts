/*!
 * Copyright © 2020 ZeroNorth, Inc. All Rights Reserved.
 *
 * All information, in plain text or obfuscated form, contained herein
 * is, and remains the property of ZeroNorth, Inc. and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to ZeroNorth, Inc. and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ZeroNorth, Inc. (support@zeronorth.io)
 *
 */
import * as path from path;
import { ApiSpec } from '@zeronorth/api-service';
import StatusController from '../src/controllers/ServiceStatus';
import v1Controllers from '../src/controllers/v1';

export default function createApiSpec(version: string): ApiSpec {
  const apiSpec: ApiSpec = {
    bodySizeLimit: '50mb',
    documentationOptions: {
      name: 'Test API',
      description: 'API microservice',
      outputDirectory: `tests/tmp/${version}`,
      entryFile: path.join(process.cwd(), 'src/controllers/v1/*.ts'),
    },
    api: [
      {
        path: version,
        controllers: v1Controllers,
      },
    ],
    statusController: StatusController,
  };

  return apiSpec;
}
