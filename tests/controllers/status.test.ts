/*!
 * Copyright © 2020 ZeroNorth, Inc. All Rights Reserved.
 *
 * All information, in plain text or obfuscated form, contained herein
 * is, and remains the property of ZeroNorth, Inc. and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to ZeroNorth, Inc. and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ZeroNorth, Inc. (support@zeronorth.io)
 *
 */
import request from 'supertest';import { container } from 'tsyringe';

import { initializeApp, Application } from '@zeronorth/api-service';
import apiSpec from '../../src/apiSpec';

describe('Service Status API', () => {
  let app: Application;const mockRepo = {
    verifyConnection: jest.fn(),
  };
  

  beforeAll(async () => {container.register('IDBStatusRepository', { useValue: mockRepo });
    
    app = await initializeApp(apiSpec);
  });

  it('Should create refined issue', async () => {
    const res = await request(app).get('/service-status');
    expect(res.status).toEqual(200);
  });
});
