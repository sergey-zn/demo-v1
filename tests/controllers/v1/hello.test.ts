/*!
 * Copyright © 2020 ZeroNorth, Inc. All Rights Reserved.
 *
 * All information, in plain text or obfuscated form, contained herein
 * is, and remains the property of ZeroNorth, Inc. and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to ZeroNorth, Inc. and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ZeroNorth, Inc. (support@zeronorth.io)
 *
 */
import request from 'supertest';
import { initializeApp, Application } from '@zeronorth/api-service';
import createApiSpec from '../../testApiSpec';

describe('Hellow API', () => {
  let app: Application;
  const version = 'v-test';

  beforeAll(async () => {
    app = await initializeApp(createApiSpec(version));
  });

  it('Should get back the provided name', async () => {
    mockRepo.getLatestJobs.mockReturnValueOnce([]);
    const res = await request(app).get(`/api/${version}/hello/test`);
    expect(res.body).toEqual({
      name: "test"
    });
    expect(res.status).toEqual(200);
  });

});
