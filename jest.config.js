/*!
 * Copyright © 2020 ZeroNorth, Inc. All Rights Reserved.
 *
 * All information, in plain text or obfuscated form, contained herein
 * is, and remains the property of ZeroNorth, Inc. and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to ZeroNorth, Inc. and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ZeroNorth, Inc. (support@zeronorth.io)
 *
 */
const {defaults} = require('jest-config');

module.exports = Object.assign({}, defaults, {
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json'
    }
  },
  preset: 'ts-jest',
  testEnvironment: 'node',
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest'
  },
  moduleFileExtensions: [
    'js',
    'ts'
  ],
  setupFiles: ['./tests/jest.setup.ts'],
  collectCoverage: true,
  coverageReporters: ['text', 'json', 'html', 'lcov'],
  reporters: ['default', './node_modules/jest-html-reporter'],
  coverageThreshold: {
    global: {
      branches: 70,
      functions: 70,
      lines: 70,
      statements: 79
    },
    './src/**/*.ts': {
      statements: 70
    }
  }
});
