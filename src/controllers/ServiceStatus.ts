/*!
 * Copyright © 2020 ZeroNorth, Inc. All Rights Reserved.
 *
 * All information, in plain text or obfuscated form, contained herein
 * is, and remains the property of ZeroNorth, Inc. and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to ZeroNorth, Inc. and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ZeroNorth, Inc. (support@zeronorth.io)
 *
 */

import { Path, GET } from 'typescript-rest';
import { Hidden } from 'typescript-rest-swagger';import { container } from 'tsyringe';
import { IDBStatusRepository } from '@zeronorth/zn-repository';


@Path('service-status')
@Hidden()
export default class ServiceController {private repo: IDBStatusRepository;

  constructor() {
    this.repo = container.resolve('IDBStatusRepository');
  }
  @GET
  public async serviceStatus(): Promise<object> {
    return Promise.resolve({
      success: true,
    });
  }
}
