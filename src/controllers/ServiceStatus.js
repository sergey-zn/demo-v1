"use strict";
/*!
 * Copyright © 2020 ZeroNorth, Inc. All Rights Reserved.
 *
 * All information, in plain text or obfuscated form, contained herein
 * is, and remains the property of ZeroNorth, Inc. and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to ZeroNorth, Inc. and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ZeroNorth, Inc. (support@zeronorth.io)
 *
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const typescript_rest_1 = require("typescript-rest");
const typescript_rest_swagger_1 = require("typescript-rest-swagger");
const tsyringe_1 = require("tsyringe");
let ServiceController = class ServiceController {
    constructor() {
        this.repo = tsyringe_1.container.resolve('IDBStatusRepository');
    }
    async serviceStatus() {
        return Promise.resolve({
            success: true,
        });
    }
};
__decorate([
    typescript_rest_1.GET
], ServiceController.prototype, "serviceStatus", null);
ServiceController = __decorate([
    typescript_rest_1.Path('service-status'),
    typescript_rest_swagger_1.Hidden()
], ServiceController);
exports.default = ServiceController;
