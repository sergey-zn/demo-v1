/*!
 * Copyright © 2020 ZeroNorth, Inc. All Rights Reserved.
 *
 * All information, in plain text or obfuscated form, contained herein
 * is, and remains the property of ZeroNorth, Inc. and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to ZeroNorth, Inc. and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ZeroNorth, Inc. (support@zeronorth.io)
 *
 */
import { Path, GET, PathParam } from 'typescript-rest';
import { Tags } from 'typescript-rest-swagger';

@Path('hello')
@Tags('hello')
export default class HelloController {
  constructor() {
    // Do any initialization here, like resolving repositories, etc. 
  }

  @Path('/:name')
  @GET
  public async getLatestJobs(@PathParam('name') name: string): Promise<any> {
    return {
      name
    }
  }
}
