"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ServiceStatus_1 = require("./controllers/ServiceStatus");
const v1_1 = require("./controllers/v1");
const apiSpec = {
    bodySizeLimit: '50mb',
    documentationOptions: {
        name: 'Issues API',
        description: 'API Service that powers Gauss-runner classification and issue creation',
        outputDirectory: 'dist/docs',
    },
    api: [
        {
            path: 'v1',
            controllers: v1_1.default,
        },
    ],
    statusController: ServiceStatus_1.default,
};
exports.default = apiSpec;
