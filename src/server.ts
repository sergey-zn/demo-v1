/*!
 * Copyright © 2020 ZeroNorth, Inc. All Rights Reserved.
 *
 * All information, in plain text or obfuscated form, contained herein
 * is, and remains the property of ZeroNorth, Inc. and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to ZeroNorth, Inc. and its suppliers
 * and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ZeroNorth, Inc. (support@zeronorth.io)
 *
 */
import { initializeApp, logger } from '@zeronorth/api-service';
import apiSpec from './apiSpec';

const initialize = async () => {
  // Any async initializations before server start, for example
  // await getContainer() from @zeronorth/zn-repository
  await Promise.resolve();
};

initialize().then(async () => {
  const app = await initializeApp(apiSpec);

  app.listen(process.env.ZN_SERVICE_PORT, () => {
    logger.info(`server started at http://localhost:${process.env.ZN_SERVICE_PORT}`);
  });
});
