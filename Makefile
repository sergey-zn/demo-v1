ENV = local
DIST = local
CUR_DIR = $(CURDIR)
NODE_VERSION = $(shell cat .nvmrc)

define GetFromJson
$(shell node -p "require('$(1)').$(2)")
endef
VERSION := $(call GetFromJson,./package.json,version)
NAME := $(call GetFromJson,./package.json,name)

clean-dist:
	npm run clean

clean-deps:
	npm run cleanDep

clean-all:
	npm run cleanAll

compile:
	@echo Compiling TypeScript to dest
	npm run build

docker_build:
	docker build -t ${NAME}:${VERSION} --build-arg NODE_VERSION=${NODE_VERSION} --build-arg GL_TOKEN=${GL_TOKEN} .

deps:
	npm install

lint:
	npm run lint

lint-fix:
	npm run format

test: deps
	npm run test


