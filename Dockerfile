ARG NODE_VERSION=14.9
ARG BASE_IMAGE=registry.gitlab.com/zeronorth/devops/build-images/zn-nodejs
ARG PLATFORM=alpine

FROM ${BASE_IMAGE}:${NODE_VERSION}-${PLATFORM} AS base
ARG CI_JOB_TOKEN
ENV CI_JOB_TOKEN=${CI_JOB_TOKEN}
WORKDIR /usr/src/zeronorth
ADD package*.json ./

FROM base AS build
ADD . ./
RUN npm install
RUN npm run build

FROM base AS prod-modules
RUN npm install --production

FROM base AS package
COPY --from=prod-modules /usr/src/zeronorth/node_modules ./node_modules
COPY --from=build /usr/src/zeronorth/dist ./dist

RUN ls -la && ls -la ./dist

CMD [ "--tls-min-v1.0", "--max-old-space-size=4096", "dist/server.js" ]